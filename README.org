*Exam-Tools:* for managing lists with student scores, and other helpers
for conducting and grading exams. At this time, perhaps many of the
tools in here are not much use to anyone except me.

* Distribute cover pages

#+begin_src
  dist-covers COVERS FORM
#+end_src

=COVERS= is a PDF file with all the cover pages, one page for each
student.

=FORM= is a PDF file with the form (or some kind of prepared pages) to
be filled out by the students, which may contain any number of pages.

For each cover page, a PDF file with the name =cover_form_NUMBER= in the
current directory will be created that starts with the cover page
followed by the form.

* Print files

#+begin_src
  print-files-simplex ...
  print-files-duplex ...
#+end_src

Print each PDF file given on the command line on the default printer,
simplex or duplex, and stapled. This is useful for printing files
generated with =dist-covers=.

* Print copies

#+begin_src
  print-copies NUM PDF
#+end_src

Prints a number of =NUM= copies of PDF file =PDF= on the default
printer, duplex and stapled. This is useful for printing assignments.

* Score to grade

#+begin_src
  score2grade L H
#+end_src

Given the minimum required score =L= and a maximal achievable score =H=,
print a transformation table to map from score to grade. The minimum
required score is the only one that gets assigned grade 4.0, the next
higher score is already 3.7, and then it goes in a linear fashion.

* Admission StudiDB

#+begin_src
  admission-studidb [--roster=FILE] [--exam-reg=FILE] --min-score NUM [--admitted=FILE] FILE ...
#+end_src

- =--roster= is a CSV file from StudiDB with data for all the
  students. Best way to create this file is by copy-and-paste from the
  web browser; do not try to save a file from StudiDB, it will probably
  have its encoding and line breaks messed up. A roster is required. If
  none is given on the command line, =roster.csv= is assumed.

- =--exam-reg= is a CSV file from the examination office, containing a
  list of students who are registered for the exam. This argument is
  optional. If it is given, a list of students will be printed to
  standard output who are registered for the exam, but not admitted.

- =--min-score= is a number giving the homework score that is required
  to be admitted.

- =--admitted= will be written to in CSV format, listing all the
  admitted students (not limited to those registered for the exam).

- The remaining files on the command line are homework score files from
  OLAT.

A student is admitted when the homework score is at least min-score or
if the number of attempts is 2 or higher (because then, it can be
assumed that the student was admitted in a previous semester). So the
list of admitted students can be longer when =--exam-reg= is used,
because the latter introduces the information about number of
attempts. For the list of admitted students that is sent to the
examination office, =--exam-reg= should not be used. It should, however,
be used to determine which students are not admitted for a particular
exam.

* Admission OLAT

Very simple version. Can only handle pass/fail, and no registration
lists. To be extended in the future. Usage:

#+begin_src
  admission-olat --min-score NUM FILE ...
#+end_src

The files should be CSV files with data from OLAT. For each student
(uniquely identified by stu number) we count the "Bestanden". Output is
a CSV formatted and sorted list of all students who have at least as
many "Bestanden" as given by =--min-score=.

* Expand template

#+begin_src
  expand-template --data FILE --template FILE
#+end_src

- ==--data== is a CSV file, where the only format currently understood
  is the one provided by =admission-olat=. (Gauche does not seem to
  provide anything to /parse/ the header of a CSV file. This is the only
  reason for this restriction at this time.)

- ==--template== is any text file, called the /template/.

For each record in the CSV file, one copy of the template is created
with the following file name: template file name without extension, two
underscores, index, two underscores, the first entry in the record
(which typically is some for of ID), and then the extension of the
template file. Each occurance of {{{KEY}}} where KEY is a column name
for the CSV file, is replaced according the values in the record.

* Manage scores

Comfortable, keyboard-based management of scores. For now just a copy
from the other repos. Needs to be generalized and documented.
